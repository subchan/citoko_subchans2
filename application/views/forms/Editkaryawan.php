<table width="47%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="pink">
<?php
	foreach ($detail_karyawan as $data){
		$nik  			= $data->nik;
		$nama_lengkap   = $data->nama_lengkap;
		$tempat_lahir   = $data->tempat_lahir;
		$tgl_lahir  	= $data->tgl_lahir;
		$jenis_kelamin  = $data->jenis_kelamin;
		$alamat  		= $data->alamat;
		$telp   		= $data->telp;
		$kode_jabatan   = $data->kode_jabatan;
		$photo			= $data->photo;
		}
		$thn_pisah = substr($tgl_lahir, 0, 4);
		$bln_pisah = substr($tgl_lahir, 5, 2);
		$tgl_pisah = substr($tgl_lahir, 8, 2);

?>

  <tr><td><center><b>Edit Karyawan</b></center><br/><br/></td></tr>
  <form action="<?=base_url();?>Karyawan/Editkaryawan/<?=$nik ;?>" method="POST" enctype="multipart/form-data">
  <dib style="color: red"><?=validation_errors(); ?></div>
  <tr>
    <td width="47%">NIK</td>
    <td width="3%">:</td>
    <td width="50%">
      <input value="<?=$nik;?>" type="text" name="nik" id="nik" maxlength="10" readonly />
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
     <input value="<?=$nama_lengkap;?>" type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50">
    </td>
  </tr>
  
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td>
      <input value="<?=$tempat_lahir;?>" type="text" name="tempatlahir" id="tempatlahir" maxlength="50">
    </td>
    
  </tr>
  
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
       <label for="jenis_kelamin"></label>
      <?php
      	if($jenis_kelamin == 'P'){
			$slc_P = 'SELECTED';
			$slc_L = '';
		}elseif($jenis_kelamin == 'L'){
			$slc_L = 'SELECTED';
			$slc_P = '';
		}else {
			$slc_P = '';
			$slc_L = '';
			}
		
	  ?>
      <select  name="jenis_kelamin" id="jenis_kelamin">
      <option <?=$slc_P;?> value="P">Perempuan</option>
      <option <?=$slc_L;?> value="L">Laki-Laki</option>
      </select>
    </td>
  </tr>
  
  
  <tr>
  <link rel ="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel ="styleshhet" href="resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <script>
$(function(){
  $("#tgl_lahir").datepicker({dateFormat: "yy-mm-dd"});  
  });
</script>
<p><input type="text" name="tgl_lahir" id="tgl_lahir"></p>
    </td>
  </tr>
  
    <tr>
    <td>Telepon</td>
    <td>:</td>
    <td>
      <input value="<?=$telp;?>" type="text" name="telepon" id="telepon" maxlength="50">
    </td>
  </tr>
  
    <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea  name="alamat" id="alamat" cols="45" rows="5" ><?=$alamat;?></textarea>
    </td>
  </tr>
  

<tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <label for="jabatan"></label>
       
      <select name="jabatan" id="jabatan">
      <?php 
	  	foreach($data_jabatan as $data) {
			$select_jabatan = ($data->kode_jabatan == $kode_jabatan) ? 'selected' : '';
			 ?>
	  <option value="<?=$data->kode_jabatan; ?>"
      <?=$select_jabatan; ?>><?=$data->nama_jabatan ;?></option>
      <?php }?>
      </select>
    </td>
  </tr>
  
  <tr>
    <td>Upload Foto</td>
    <td>:</td>
    <td>
      <input type="file" name="image" id="image">
      <input type="hidden" name="foto_old" id="foto_old" value="<?= $photo; ?>">
    </td>
  </tr>
  
 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Simpan" id="Simpan" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
    </td>
  </tr>
  
  
  
  
</table>
</form>
